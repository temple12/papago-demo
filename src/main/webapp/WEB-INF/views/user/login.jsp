<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
</head>
<body>
	<input type="text" name="id" id="id"> 
	<input type="text" name="pwd" id="pwd">
	<button onclick="doLogin()">로그인</button>
	
	<script>
		function doLogin(){
			var data = {
					id:$('#id').val(),
					pwd:$('#pwd').val()
			}
			$.ajax({
				method:'POST',
				url:'/user/login',
				data:JSON.stringify(data),
				contentType:'application/json',
				success:function(res){
					console.log(res);
				}
			})
		}
	</script>
</body>
</html>