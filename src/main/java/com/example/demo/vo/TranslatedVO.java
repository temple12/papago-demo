package com.example.demo.vo;

import lombok.Data;

@Data
public class TranslatedVO {
	private String srcLangType;
	private String tarLangType;
	private String translatedText;
	private String engineType;
	private String pivot;
}
